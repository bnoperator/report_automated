---
title: "Supplement"
author: ""
date: ""
params:
  objective: "kinases implicated in cell line mutations"
  type: "PTK STK"
  stats: "paired t-test"
output:
  word_document:
    reference_docx: report_ref.docx
    toc: yes
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
```

```{r warning=FALSE, message=FALSE}
source("R//functions.R")
```

<!-- type can be "PTK" or "STK" or "PTK STK" or "PTP" -->
<!-- type can be "t-test" or "paired t-test" or "multiple treatment versus control" -->

#########

# Introduction

The supplement provides a detailed explanation of the methods used in the main study report. Each section is standalone.

## Sample Annotation

The experiment was annotated in order to facilitate the BioNavigator analysis:

Outline the annotation.

## Image Analysis 

The image analysis is performed on each image.
Once a grid is contructed for the image then 23 quantitation types are calculated. The two most used quantitation types are 

* `Median_SigmBg`
* `Signal_Saturation`

A local backgroud correction is used per spot.

__PTK image__:

```{r results = 'asis'}
image_and_text("01_image_analysis_PTK","array_images.png")
```

__STK image__:

```{r results = 'asis'}
image_and_text("01_image_analysis_STK","array_images.png")
```

#########

## Exposure Time Integration

If the same image has been acquired at multiple exposure times, combining the measurements of the images in a single value increases the dynamic range of the measurements (the ratio of the highest and lowest signal that can be measured).

This is useful in the common case that there are both low intensity spots and high intensity spots.
With the current instrument protocols multiple exposure times are applied after the final washing step (After Washing).
Standard way of combining multiple exposure times is to take the slope of a linear fit of the Median_SigmBg as a function of exposure time, while excluding those measurements with a Signal_Saturation value larger than 0.05.
The tool which performes this integration is called `PamApp for Exposure Time Scaling`.


#########

## Quality Control

Three levels of controls were checked.

* peptide signal control (i.e. what signal strength do the peptide)
* peptide number control (i.e how many peptides passed)
* run control (i.e. is overall experiment variation acceptable)
* sample quality control (i.e. what samples should be retained)

A flag system is used to indicate a quality level. The quality flag address the two items above (run and peptide signal control ). 
 
|Level |Flags                        | Signal   | No. (PTK) | No. (STK) | CV(TR)  | CV(BR)
|:--   |:---                         | :---     | :---      | :---      | :---   | :---
|High  |![](99_images\\green_10.png) | 0-4,000  | >123      | >90       | <20%   | <30%
|Middle|![](99_images\\orange_10.png)| 0-2,000  | 78-123    | 56-90     | 20%-30%| 30%-40%
|Low   |![](99_images\\red_10.png)   | 0-1,000  | <78       | <56       | >30%   | >40%

The table above gives the criteria values for the different flags. The explanation of the column headers are given:

__Signal__:

Typical signal values of S100 are: 0 - 4,000 AU (arbitrary units)

__CV(TR)__:

CV of TR (Technical Replicates),  1 lysate/aliquot on 3+ arrays.

__CV(BR)__:

CV of BR (Biological replicates), 1 lysate per array.

Each QC control aspects are dealt individually in the sections below.

#########

### Experimental Variation

Experimental variation is assessed by calculating the CV (Coefficient of Variance), either biological or technical).
The current experiment has a value ![](99_images\\orange_10.png) and it considered acceptable/not acceptable ![](99_images\\X.png).

This is plotted to indicating overall experimental variation, it is used to determine the quality of the run(s).

__PTK CV plot__:

![](02_cv_PTK\\cv.png)

#########

__STK CV plot__:

![](02_cv_STK\\cv.png)

#########

### Peptide Quality Control (PTK)

For the PTK Assay there is a kinetic readout on our PamChip. The following graphics show the kinetic readout per peptide per array.

```{r results = 'asis'}
image_add_legend_text("03_kinetic_QC_PTK","Kinetic QC.png", "legend.png")
```


The values from the images obtained with different exposure times for each Cycle were integrated to one value. The phosphorylation kinetics for each peptide (each spot) was analyzed.  

Peptide QC selection: Only peptides that showed kinetics (increase of signal in time) on at least 25 % of the arrays were included in the analysis (kinetic fraction present > 0.25). ![](99_images\\orange_10.png) peptides were retained for further analysis. Values after wash (Cycle 94; for the QC peptides) were log transformed and values below 0 were removed.

#########

### Peptide Quality Control (STK)

The values from the images obtained with different exposure times for Cycle 124 were integrated to one value. Peptide selection: Nominal CV (Coefficient of variation) was calculated per peptide using a 2-component error fit model using overall mean as input and was used as a filter to remove low-intensity spots. Only peptides that showed nominal CV <0.5 were included in the analysis. ![](99_images\\X.png) peptides were retained for further analysis.  Values after wash (Cycle 124; for the QC peptides) were log transformed and values below 1 were shifted to 1 to account for negative values.

```{r results = 'asis'}
image_add_legend_text("03_pepQC_STK","pepQC.png", "legend.png")
```

#########

## Normalization

## Differential Analysis of Phosphosite

## Biological Interpretation

The current approaches for biological interpretation are:

* differential kinase interpretation
* pathway analysis
* network analysis

## Kinase Interpretation

## Pathway and Network Interpretation

A pathway analysis is performed using MetaCore (Clarivate Analysis).

## PamChip Technology Outline

![](20_background\\technology\\Slide1.PNG)

## Multiplex Kinase Readouts Outline

![](20_background\\technology\\Slide2.PNG)

#########

# Glossary

The following entries are defined to help in reading the report:

PamChip
  ~ The name of the chip, there are four arrays on one PamChip. There PamChips can be loaded in one PamStation12 run.

Array
  ~ one well, there are four arrays on a PamChip.

PTK, STK and PTP
  ~ These are abbreviations for the type of PamChips.PTK is tyrosine [see link](https://pamcloud.pamgene.com/wiki/Wiki.jsp?page=PTK) STK is the serine threonine [see link](https://pamcloud.pamgene.com/wiki/Wiki.jsp?page=STK), PTP is the phosphatase [see link](https://pamcloud.pamgene.com/wiki/Wiki.jsp?page=PTP).

Median_SigmBg
  ~ standard signal

S100
  ~ is the signal at 100 times the slope calculated using multiple exposure times of the Median_SigmBg. It is calculated in the PamApp for Exposure Time Scaling [see link](https://pamcloud.pamgene.com/wiki/Wiki.jsp?page=S100).
  
after wash 
  ~  Refers to all steps in an instrument protocols which succeed the "wash step". An instrument step used to remove all excess analyte. It typically represents the second half of the readouts of an experiment, i.e. kinetics. The first halve of the readouts is referred to as pre-wash [see link](https://pamcloud.pamgene.com/wiki/Wiki.jsp?page=After_wash).
  
pre-wash
  ~ Refers to all steps in an instrument protocols which precede the "wash step". An instrument step used to remove all excess analyte. For the kinase assay, it typically represents the first halve of the readouts of an experiment, i.e. kinetics. The second half of the readouts are referred to as After-wash.
Typically, images taken in this phase will progressively increase in brightness as the pump cycle count increases [see link](https://pamcloud.pamgene.com/wiki/Wiki.jsp?page=Pre_wash).
  

log fold change (lfc)
  ~ a calculation of the effect size in the log2 scale.

volcano plot
  ~ a plot showing size and significance of experimental intervention (i.e. effect)

paired t-test
  ~ paired on barcode t-test

t-test
  ~ standard t-test

control vs multiple treatment
  ~ type of anova (with a post-hoc dunnet)

kinase interpretation
  ~ associating the upstream kinases to the phosphosites (i.e. peptides)
  
technical replicate
  ~ the exact same lysate
  
biological replicate
  ~ the lysate prepared from a different source, different mouse or different passage number
  

