# Intro

Semi-automated reporting of kinase PamChip measurements. It comes with useful automated reports.
Currently there are three main types of reports

* `study.Rmd` (the main report)
* `kinase_tables.Rmd` (report with only kinase tables)
* `figures.Rmd`  (report with only figures)

# Main report

The main report is defined by the `study.Rmd` and `appendix.Rmd` file.

To use the main report please.

1. Pull the repository
2. Copy the complete `report_automated` folder project to a customer project
3. Save the images to the appropiate folders in project
4. Double click on the `report_automated.Rproj` to start RStudio
4. Modify the report file `study.Rmd` and `supplement_static.Rmd` or `supplement_dynamic.Rmd`
5. Use the __Knit button__ in RStudio to regenerate both reports

# Kinase tables report

To generate a report with only kinase tables and plots and many of them.

1. Put the SummaryTables into the folders:
     * `10_UpstreamKinase_PTK_tables`
     * `10_UpstreamKinase_PTK_tables`
2. Double click on the `report_automated.Rproj` to start RStudio
3. Use the __Knit button__ in RStudio to generate the report

# Figures report

1. Put the figures into the folders:
     * `11_Figures_PTK`
     * `11_Figures_STK`
2. Double click on the `report_automated.Rproj` to start RStudio
3. Use the __Knit button__ in RStudio to generate the report

